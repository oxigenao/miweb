'use strict';

/**
 * @ngdoc function
 * @name pabloWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pabloWebApp
 */
angular.module('pabloWebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
