'use strict';

/**
 * @ngdoc function
 * @name pabloWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pabloWebApp
 */
angular.module('pabloWebApp')
  .controller('MainCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.moreSkilss = false;
  });
